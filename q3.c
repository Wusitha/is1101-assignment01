#include <stdio.h>

int main(void) {

  int a;
  int b;
  int store;

  //get inputs
  printf("Enter number 1: ");
  scanf("%d", &a);
  printf("Enter number 2: ");
  scanf("%d", &b);

  printf("Before swap\n");
  printf("Number 1= %d\n", a);
  printf("Number 2= %d\n", b);

  //swap
  store = a;
  a = b;
  b= store;

  printf("After swap\n");
  printf("Number 1= %d\n", a);
  printf("Number 2= %d\n", b);

  return 0;

}