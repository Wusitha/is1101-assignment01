#include <stdio.h>

int main(void) {

  //variables
  float num1 = 1.5;
  float num2 = 2;

  //calculate sum
  float sum = num1 + num2;

  //print sum
  printf("Sum: %.2f\n", sum);
  return 0;
}