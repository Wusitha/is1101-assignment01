#include <stdio.h>

int main(void) {

  float r;
  float area;

  //get input from keyboard
  printf("Enter radius: ");
  scanf("%f", &r);

  //calculate area
  area = (22.0 / 7) * r * r;

  //display area
  printf("Area: %.2f\n", area);

  return 0;
}